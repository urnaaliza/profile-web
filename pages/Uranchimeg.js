import Image from "next/image";

export default function Uranchimeg() {
  return (
    <div className="w-screen h-screen  bg-gradient-to-r from-green-200 via-cyan-400 to-blue-200  flex justify-center items-center">
       <div className="absolute z-0 w-24 h-24 rounded-full bg-gradient-to-tr from-slate-50 to-cyan-300 top-[15%] left-[80%] bottom-[50%]" ></div>
       <div className="absolute z-0 w-24 h-24 rounded-full bg-gradient-to-tr from-slate-200 to-cyan-400 top-[75%] right-[80%] " ></div>
      <div className="w-[80%] h-[60%]  flex justify-center gap-2 rounded-xl ">
        {/* 1 deh  */}
        <div className="w-full h-full flex  ">
          <div className="w-[40%] h-full  px-10 py-5 bg-slate-200 bg-opacity-50 backdrop-blur-md rounded-xl">
            {/* zurag */}
            <div className=" bg-yellow-50 bg-opacity-50 backdrop-blur-md mx-auto w-24 h-24 rounded-full mt-4 items-center justify-center flex ">
              <img
                src="62.jpg"
                className="w-20 h-20 rounded-full object-cover "
              />
            </div>
            {/* ners */}
            {/* <div className="w-full h-full bg-slate-200  ">  </div>*/}
            <div className="w-full h-10 flex font-medium  items-center   text-lg  text-cyan-900  text-center ml-5">
              Ya.Uranchimeg
            </div>
            <div className="w-full  h-15 flex items-center font-medium gap-4  text-xs  border-cyan-50/20 rounded-2xl   text-cyan-600 hower:text-2xl text-center ">
              General Tax Of Mongolian / Programmer
            </div>
            <div className="w-full flex h-10 items-center gap-4 text-sm  border-slate-50/20 rounded-2xl  font-bold text-cyan-800 hover:bg-slate-300 hower:text-2xl text-center mt-5">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 24 24"
                fill="currentColor"
                className="w-6 h-6 text-cyan-500"
              >
                <path d="M11.7 2.805a.75.75 0 01.6 0A60.65 60.65 0 0122.83 8.72a.75.75 0 01-.231 1.337 49.949 49.949 0 00-9.902 3.912l-.003.002-.34.18a.75.75 0 01-.707 0A50.009 50.009 0 007.5 12.174v-.224c0-.131.067-.248.172-.311a54.614 54.614 0 014.653-2.52.75.75 0 00-.65-1.352 56.129 56.129 0 00-4.78 2.589 1.858 1.858 0 00-.859 1.228 49.803 49.803 0 00-4.634-1.527.75.75 0 01-.231-1.337A60.653 60.653 0 0111.7 2.805z" />
                <path d="M13.06 15.473a48.45 48.45 0 017.666-3.282c.134 1.414.22 2.843.255 4.285a.75.75 0 01-.46.71 47.878 47.878 0 00-8.105 4.342.75.75 0 01-.832 0 47.877 47.877 0 00-8.104-4.342.75.75 0 01-.461-.71c.035-1.442.121-2.87.255-4.286A48.4 48.4 0 016 13.18v1.27a1.5 1.5 0 00-.14 2.508c-.09.38-.222.753-.397 1.11.452.213.901.434 1.346.661a6.729 6.729 0 00.551-1.608 1.5 1.5 0 00.14-2.67v-.645a48.549 48.549 0 013.44 1.668 2.25 2.25 0 002.12 0z" />
                <path d="M4.462 19.462c.42-.419.753-.89 1-1.394.453.213.902.434 1.347.661a6.743 6.743 0 01-1.286 1.794.75.75 0 11-1.06-1.06z" />
              </svg>
              Education
            </div>
            <div className="w-full flex h-10 items-center gap-4 text-sm  border-slate-50/20 rounded-2xl  font-bold text-cyan-800 hover:bg-slate-300 hower:text-2xl text-center mt-5">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 24 24"
                fill="currentColor"
                className="w-6 h-6 text-cyan-500"
              >
                <path
                  fillRule="evenodd"
                  d="M11.828 2.25c-.916 0-1.699.663-1.85 1.567l-.091.549a.798.798 0 01-.517.608 7.45 7.45 0 00-.478.198.798.798 0 01-.796-.064l-.453-.324a1.875 1.875 0 00-2.416.2l-.243.243a1.875 1.875 0 00-.2 2.416l.324.453a.798.798 0 01.064.796 7.448 7.448 0 00-.198.478.798.798 0 01-.608.517l-.55.092a1.875 1.875 0 00-1.566 1.849v.344c0 .916.663 1.699 1.567 1.85l.549.091c.281.047.508.25.608.517.06.162.127.321.198.478a.798.798 0 01-.064.796l-.324.453a1.875 1.875 0 00.2 2.416l.243.243c.648.648 1.67.733 2.416.2l.453-.324a.798.798 0 01.796-.064c.157.071.316.137.478.198.267.1.47.327.517.608l.092.55c.15.903.932 1.566 1.849 1.566h.344c.916 0 1.699-.663 1.85-1.567l.091-.549a.798.798 0 01.517-.608 7.52 7.52 0 00.478-.198.798.798 0 01.796.064l.453.324a1.875 1.875 0 002.416-.2l.243-.243c.648-.648.733-1.67.2-2.416l-.324-.453a.798.798 0 01-.064-.796c.071-.157.137-.316.198-.478.1-.267.327-.47.608-.517l.55-.091a1.875 1.875 0 001.566-1.85v-.344c0-.916-.663-1.699-1.567-1.85l-.549-.091a.798.798 0 01-.608-.517 7.507 7.507 0 00-.198-.478.798.798 0 01.064-.796l.324-.453a1.875 1.875 0 00-.2-2.416l-.243-.243a1.875 1.875 0 00-2.416-.2l-.453.324a.798.798 0 01-.796.064 7.462 7.462 0 00-.478-.198.798.798 0 01-.517-.608l-.091-.55a1.875 1.875 0 00-1.85-1.566h-.344zM12 15.75a3.75 3.75 0 100-7.5 3.75 3.75 0 000 7.5z"
                  clipRule="evenodd"
                />
              </svg>
              Skills
            </div>
            <div className="w-full flex h-10 items-center gap-4 text-sm  border-slate-50/20 rounded-2xl  font-bold text-cyan-800 hover:bg-slate-300 hower:text-2xl text-center mt-5">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 24 24"
                fill="currentColor"
                className="w-6 h-6 text-cyan-500"
              >
                <path d="M1.5 8.67v8.58a3 3 0 003 3h15a3 3 0 003-3V8.67l-8.928 5.493a3 3 0 01-3.144 0L1.5 8.67z" />
                <path d="M22.5 6.908V6.75a3 3 0 00-3-3h-15a3 3 0 00-3 3v.158l9.714 5.978a1.5 1.5 0 001.572 0L22.5 6.908z" />
              </svg>
              Experience
            </div>
            <div className="w-full flex h-10 items-center gap-4 text-sm  border-slate-50/20 rounded-2xl  font-bold text-cyan-800 hover:bg-slate-300 hower:text-2xl text-center   mt-5">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 24 24"
                fill="currentColor"
                className="w-6 h-6 text-cyan-400 "
              >
                <path d="M11.25 4.533A9.707 9.707 0 006 3a9.735 9.735 0 00-3.25.555.75.75 0 00-.5.707v14.25a.75.75 0 001 .707A8.237 8.237 0 016 18.75c1.995 0 3.823.707 5.25 1.886V4.533zM12.75 20.636A8.214 8.214 0 0118 18.75c.966 0 1.89.166 2.75.47a.75.75 0 001-.708V4.262a.75.75 0 00-.5-.707A9.735 9.735 0 0018 3a9.707 9.707 0 00-5.25 1.533v16.103z" />
              </svg>
              Project
            </div>
            <div className="mt-4 mb-4 items-center  flex  justify-center gap-2">
              <div className="border-slate-50/40 border-2 bg-slate-100 bg-opacity-20 backdrop-blur-md w-8 h-8 rounded-full  items-center justify-center flex">
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor" className="w-6 h-6">
  <path fillRule="evenodd" d="M19.5 9.75a.75.75 0 01-.75.75h-4.5a.75.75 0 01-.75-.75v-4.5a.75.75 0 011.5 0v2.69l4.72-4.72a.75.75 0 111.06 1.06L16.06 9h2.69a.75.75 0 01.75.75z" clipRule="evenodd" />
  <path fillRule="evenodd" d="M1.5 4.5a3 3 0 013-3h1.372c.86 0 1.61.586 1.819 1.42l1.105 4.423a1.875 1.875 0 01-.694 1.955l-1.293.97c-.135.101-.164.249-.126.352a11.285 11.285 0 006.697 6.697c.103.038.25.009.352-.126l.97-1.293a1.875 1.875 0 011.955-.694l4.423 1.105c.834.209 1.42.959 1.42 1.82V19.5a3 3 0 01-3 3h-2.25C8.552 22.5 1.5 15.448 1.5 6.75V4.5z" clipRule="evenodd" 
  className="w-6 h-6 text-cyan-400 " />
</svg>

              </div>
              <a href="https://www.google.com/maps " target="_blank" className="border-slate-50/40 border-2 bg-slate-50 bg-opacity-20 backdrop-blur-md w-8 h-8 rounded-full  items-center justify-center flex">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  viewBox="0 0 24 24"
                  fill="currentColor"
                  className="w-4 h-4 text-cyan-400"
                >
                  <path
                    fillRule="evenodd"
                    d="M11.54 22.351l.07.04.028.016a.76.76 0 00.723 0l.028-.015.071-.041a16.975 16.975 0 001.144-.742 19.58 19.58 0 002.683-2.282c1.944-1.99 3.963-4.98 3.963-8.827a8.25 8.25 0 00-16.5 0c0 3.846 2.02 6.837 3.963 8.827a19.58 19.58 0 002.682 2.282 16.975 16.975 0 001.145.742zM12 13.5a3 3 0 100-6 3 3 0 000 6z"
                    clipRule="evenodd"
                  />
                </svg>
              </a>
              <div className="border-slate-50 border-2 bg-slate-50 bg-opacity-20 backdrop-blur-md w-8 h-8 rounded-full items-center justify-center flex">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  viewBox="0 0 24 24"
                  fill="currentColor"
                  className="w-4 h-4 text-cyan-400"
                >
                  <path
                    fillRule="evenodd"
                    d="M12 2.25c-5.385 0-9.75 4.365-9.75 9.75s4.365 9.75 9.75 9.75 9.75-4.365 9.75-9.75S17.385 2.25 12 2.25zM6.262 6.072a8.25 8.25 0 1010.562-.766 4.5 4.5 0 01-1.318 1.357L14.25 7.5l.165.33a.809.809 0 01-1.086 1.085l-.604-.302a1.125 1.125 0 00-1.298.21l-.132.131c-.439.44-.439 1.152 0 1.591l.296.296c.256.257.622.374.98.314l1.17-.195c.323-.054.654.036.905.245l1.33 1.108c.32.267.46.694.358 1.1a8.7 8.7 0 01-2.288 4.04l-.723.724a1.125 1.125 0 01-1.298.21l-.153-.076a1.125 1.125 0 01-.622-1.006v-1.089c0-.298-.119-.585-.33-.796l-1.347-1.347a1.125 1.125 0 01-.21-1.298L9.75 12l-1.64-1.64a6 6 0 01-1.676-3.257l-.172-1.03z"
                    clipRule="evenodd"
                  />
                </svg>
              </div>
            </div>
          </div>

          {/*2 doh bagana  */}


          <div className="w-full h-full  py-8 px-8 bg-cyan-50  bg-opacity-50  backdrop-blur-2xl rounded-xl ">
            
            <div className="flex ">
          <div className="text-Bold py-2 text-3xl ml-10 text--900 font-extrabold  rounded-full">
    HeLLo! </div>
    <div className="flex items-center justify-end">
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor" className="w-8 h-8">
  <path d="M10.5 1.875a1.125 1.125 0 012.25 0v8.219c.517.162 1.02.382 1.5.659V3.375a1.125 1.125 0 012.25 0v10.937a4.505 4.505 0 00-3.25 2.373 8.963 8.963 0 014-.935A.75.75 0 0018 15v-2.266a3.368 3.368 0 01.988-2.37 1.125 1.125 0 011.591 1.59 1.118 1.118 0 00-.329.79v3.006h-.005a6 6 0 01-1.752 4.007l-1.736 1.736a6 6 0 01-4.242 1.757H10.5a7.5 7.5 0 01-7.5-7.5V6.375a1.125 1.125 0 012.25 0v5.519c.46-.452.965-.832 1.5-1.141V3.375a1.125 1.125 0 012.25 0v6.526c.495-.1.997-.151 1.5-.151V1.875z" 
  className="text-indigo-400"
  />

</svg>

</div>
    
    </div>
    <div className="text-Bold  text-2xl ml-10 text-zinc-700 font-extrabold  rounded-full ">
    My self
</div>


            <div className="w-full h-30 text-sm text-justify  font-bold text-gray-500  mt-4 ">
              Hey, this is my homepage, so I have to say something about myself.
              Sometimes it is hard to introduce yourself because you know
              yourself so well that you do not know where to start with. Let me
              give a try to see what kind of image you have about me through my
              self-description. I hope that my impression about myself and your
              impression about me are not so different. Here it goes.
            </div>
            <div className="w-full h-30 text-sm text-justify   font-bold text-gray-500  mt-4">
              Let me give a try to see what kind of image you have about me through my
              self-description. I hope that my impression about myself and your
              impression about me are not so different. Here it goes.
            </div>

            <div className="w-full h-30  text-sm  text-justify   font-bold text-gray-500  mt-4">
              This is a brief introduction of myself. If you are interested in
              knowing more, read my articles or take a look at my pictures. Do
              not expect too much, and keep your sense of humor.
              <div  className=" w-full h-10 flex rounded-xl justify-end  mt-6 text-cyan-700 " >
            <a href="https://www.youtube.com " target="_blank" className="w-60 h-10 flex justify-center items-center text-center bg-opacity-50  backdrop-blur-xl rounded-xl " > View My CV (Mongolian)
          
            </a>
            </div>
            </div>
           
          </div>
          
        </div>
        
      </div>
      
    </div>
    
  );
}
